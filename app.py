from flask import Blueprint
from flask_restful import Api
from services.service import Service

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

api.add_resource(Service, '/counter')
