from flask import Flask, request
from flask_restful import Resource, Api


def create_app():
    app = Flask(__name__)
    from app import api_bp
    app.register_blueprint(api_bp,url_prefix = '/api')

    return app


if __name__ == '__main__':

    app = create_app()
    app.run(host='127.0.0.1',port='5000',threaded=True,debug=False)
