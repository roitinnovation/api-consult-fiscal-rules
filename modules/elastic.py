import json
from elasticsearch import Elasticsearch

class Elastic:
    def __init__(self):
        with open("./config/config.json") as f:
            self.cred = json.load(f)
        
    def consult(self, query_p, index_p, doc_type_p = None):
    
        url_p = self.cred['url']
        port_p = self.cred['port']
        user_p = self.cred['user']
        pass_p = self.cred['password']
        es = Elasticsearch(host = url_p, port = port_p,scheme="https", http_auth=(user_p, pass_p))
        res = es.count(body = query_p, index = index_p, doc_type = doc_type_p, request_timeout=10000)
	      
        return res