from elasticsearch import Elasticsearch
from modules.elastic import Elastic

class Count:
	
    def __init__(self):
           pass
           
    def Routine(self, owner_id):
	   
        query_p = {
            "query": {
                "bool": {
                    "must": [
                        {"match": {
                        "FONTE": owner_id
                        }}
                    ]
                }
            }
        }	
           
        if owner_id == "1":
            consult = Elastic().consult(query_p, index_p='regrasfiscais')
        else:				
            consult = Elastic().consult(query_p, index_p='regrasfiscaisclientes')
		        
        return consult
