import json
from flask import Flask, request
from flask_restful import Resource, Api
from routines.routine import Count

class Service(Resource):

    def __init__(self):
        pass
    
    def get(self):
        return("Get method its not supported"),404

    def post(self):
        payload = request.get_json(force=True)
        res = Count().Routine(payload['FONTE'])
    
        c = res.get('count')
        return {"status":"success", "message":"Success at find count", "data": c, "error": None},200